import Ampoule from "@/components/question/ampoule/ampoule.vue";
export default {
	components: {Ampoule},
	props: {
		test: null,
		résultat: null,
		panneauAffiché: null,
	},
};