export default {
	name: "FenêtreInfo",
	computed: {
		thèmeSombre() {
			return this.$store.state.thèmeSombre;
		}
	}
};
