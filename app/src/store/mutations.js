export const mutations = {
	setErreurs(state, erreurs) {
		state.erreurs = erreurs;
	},

	setUser(state, user) {
		state.user = user;
	},

	setUsername(state, username) {
		state.username = username;
	},

	setToken(state, token) {
		state.token = token;
	},
	
	setTokenRessources(state, tokenRessources) {
		state.tokenRessources = tokenRessources;
	},

	setUri(state, uri) {
		state.uri = uri;
	},

	setDémo(state, val) {
		state.démo = val;
	},

	setLangageDéfaut(state, langageDéfaut) {
		state.langageDéfaut = langageDéfaut;
	},

	setCallbackSucces(state, cb_succes) {
		state.cb_succes = cb_succes;
	},

	setCallbackSuccesParams(state, cb_succes_params) {
		state.cb_succes_params = cb_succes_params;
	},

	setCallbackAuth(state, cb_auth) {
		state.cb_auth = cb_auth;
	},

	setCallbackAuthParams(state, cb_auth_params) {
		state.cb_auth_params = cb_auth_params;
	},

	setConfigServeur(state, config) {
		state.configServeur = config;
	},

	setAvancement(state, avancement) {
		state.avancement = avancement;
	},
	setTentative(state, tentative) {
		state.tentative = tentative;
	},
	setQuestion(state, question) {
		state.question = question;
	},
	updateCodeTentative(state, code) {
		state.tentative.code = code;
	},
	updateLangageTentative(state, langage) {
		state.tentative.langage = langage;
	},
	updateEnvoieTentativeEnCours(state, bool) {
		state.envoiTentativeEnCours = bool;
	},
	updateAuthentificationEnCours(state, bool) {
		state.authentificationEnCours = bool;
	},
	setSauvegarde(state, sauvegarde) {
		state.sauvegardes[sauvegarde.langage] = sauvegarde;
	},
	setSauvegardes(state, sauvegardes) {
		state.sauvegardes = sauvegardes;
	},
	setDifficultésRéussies(state, difficultésRéussies) {
		state.difficultésRéussies = difficultésRéussies;
	},
	setThèmeSombre(state, val) {
		state.thèmeSombre = val;
	},
	setNbRéussitesParLangage(state, nbRéussitesParLangage) {
		state.nbRéussitesParLangage = nbRéussitesParLangage;
	},

	setModeAffichage(state, val) {
		state.mode_affichage = val;
	},
	setSélectionnerTestHaut(state, val){
		state.sélectionnerTestHaut = val;
	},
	setSélectionnerTestBas(state, val){
		state.sélectionnerTestBas = val;
	},
	setChangerModeAffichageAvecRaccourci(state, val){
		state.changerModeAffichageAvecRaccourci = val;
	},
	setIndicateursDeFonctionnalité(state, val){
		state.indicateursDeFonctionnalité = val;
	},
	setEnChargement(state, val){
		state.enChargement = val;
	},
	setEntréeTest(state, val) {
		state.question.tests[val.index].entrée = val.entrée;
	},
	setParamsTest(state, val) {
		state.question.tests[val.index].params = val.params;
	},
	setTests(state, val) {
		state.question.tests = val;
	},
	setRésultat(state, params){
		state.tentative.resultats[params.index] = params.résultat;
	},
	setRésultats(state, val){
		state.tentative.resultats = val;
	},
	setFeedback(state, val){
		state.tentative.feedback = val;
	}
};
