{
	"erreur": {
		"réseau": "Connection to server failed. Please retry or contact an administrator.",
		"authentification": "Incorrect username or password.",
		"inscription": "That username is already taken.",
		"tentative_intraitable": "The attempt is invalid. The question may be invalid or may have changed since your draft was loaded. Try reloading the initial draft. If the problem persists, contact an administrator.",
		"question_introuvable": "The question does not exist or could not be retreived. If the problem persists, contact the author of the question."
	},

	"menu": {
		"accomplissement":"Achievements",
		"connexion": "Login",
		"déconnexion": "Logout",
		"thèmeSombre": "Dark theme",
		"thèmeClair": "Light theme"
	},

	"accomplissements": {
		"titrePage": "My Achievements",
		"titreQuestion": "Title",
		"difficulté": "Difficulty level",
		"dateDeDernièreTentative": "Last attempt",
		"dateDeRéussite": "Resolved",
		"titreGraphParLangage": "Achievements by language",
		"titreGraphParDifficulté": "Achievements by difficulty"
	},

	"login": {
		"courriel": "Username",
		"motDePasse": "Password",
		"boutonConnexion": "Log in",
		"authorisation": "Authorise the plateform {plateforme} to connect to Progression in your name for the course {cours} : ",
		"persister": "Remember me on this computer",
		"standard": "Standard",
		"inscription": "Sign in",
		"usernameInvalide": "The username must contain only letters, dashes et underscores.",
		"champObligatoire": "Mandatory field",
		"mdpOublié": "forgot your password?"
	},


	"présentation": {
		"passer": "Skip tutorial",
		"suivant": "Next",
		"précédent": "Previous",
		"terminer": "End tutorial",
		"bienvenue": {
			"titre": "Welcome",
			"contenu": "Welcome to Progression, the programming gym<br>Follow the guide!"
		},
		"énoncé": {
			"titre": "Statement",
			"contenu": "The statement of the task is in this section."
		},
		"titre": {
			"titre": "Statement",
			"contenu": "The title will be marked by a green checkmark once completed."
		},
		"niveau":{
			"titre": "Statement",
			"contenu": "The difficulty level of the task."
		},
		"énoncé_détail": {
			"titre": "Statement",
			"contenu": "The current task is described here :<ul><li>inputs<li>outputs<li>examples<li>etc.</ul> Read carefully!"
		},
		"code":{
			"titre": "Code editor",
			"contenu": "The code editor. This is where you get at work!"
		},
		"pref_affichage": {
			"titre": "Code editor",
			"contenu": "Display preferences. Are you more of a night owl or an early bird?"
		},
		"ébauche":{
			"titre": "Code editor",
			"contenu": "Complete the suggested code here. Some lines are hidden, others are deactivated to keep you focused on your task."
		},
		"validation":{
			"titre": "Validation",
			"contenu": "You think you have completed the task? Validate your answer by clicking here."
		},
		"progression":{
			"titre": "Progression",
			"contenu": "Your progress for this task. If everything shows up green, you have succeeded!"
		},
		"rétroaction":{
			"titre": "Feedback",
			"contenu": "For tips, watch for the 💡 that will appear here."
		},
		"jeu_tests":{
			"titre": "Tests",
			"contenu": "All of these tests must be validated in order for the task to be marked as completed."
		},
		"entrées":{
			"titre": "Tests",
			"contenu":"Each test provides the program with different inputs."
		},
		"sorties_attendues":{
			"titre": "Tests",
			"contenu": "and makes sure it has received specific outputs."
		},
		"sorties_observées":{
			"titre": "Tests",
			"contenu": "Your program's outputs must match exactly the ones expected!"
		},
		"avancement":{
			"titre": "Select a draft",
			"contenu": "Use this menu to switch between programming languages..."
		},
		"ébauche_initiale": {
			"titre": "Select a draft",
			"contenu": "to return to the initial draft for this language..."
		},
		"tentative":{
			"titre": "Select a draft",
			"contenu": "or to a previous attempt."
		},
		"fin":{
			"titre": "It's time to work",
			"contenu": "And now, it's your turn!"
		}
	},

	"inscription": {
		"username": "Choose a username",
		"confirmation": "Confirm",
		"boutonInscription": "Sign in",
		"mdpDifférents": "Passwords must match."
	},

	"énoncé": {
		"auteur": "Author",
		"licence": "License"
	},

	"validation_tentative": {
		"boutonValider": "Validate"
	},

	"jeu_tests": {
		"jeuTests": "Test cases"
	},

	"mode_affichage": "Diff",

	"editeur": {
		"langageÉbauche": "Language : ",
		"choixLangage": "-- Select a language --",
		"réinitialiser": "Reset",
		"réinitialiser_avertissement": "Changes made since the last attempt will be lost. Do you want to reset?"
	},

	"retroaction_tentative": {
		"bonneRéponse": "Correct",
		"mauvaiseRéponse": "Incorrect",
		"nbTestsValidés": "Passed tests : ",
		"conseil": "Tip : ",
		"erreurServeur": "The test server in inaccessible. If the problem persists, please notify an administrator."
	},

	"resultat_test": {
		"entrée": "Input",
		"sortieAttendue": "Expected output",
		"params": "Arguments",
		"sortieErreur": "Error output",
		"sortieConsole": "Actual output",
		"rétroaction": "Feedback",
		"vide": "[empty]"
	},

	"resultat_details": {
		"sectionDétails": "Details",
		"tempsTest": "Running time of test:",
		"unitéMesure": "ms"
	},

	"avancement": {
		"premièreTentative": "This is your first attempt. You can do it!",
		"questionNonRésolue": "Not solved yet. Keep trying.",
		"questionRésolue": "Solved! Good job!",
		"questionIndéterminée": "This exercice is... -/+@-/*»!... I can't hear you, I'm using the scrambler!",
		"versionTentative": "Previous attempts : ",
		"choisirTentative": "Choose a previous attempt",
		"dateTentative": "Attempt from ",
		"ébauche_initiale": "Reset initial draft"
	},

	"onglets_informations": {
		"entrées/sorties": "Input / Output",
		"erreurs": "Errors",
		"rétroactions": "Feedback",
		"détails": "Details"
	},

	"commentaire": {
		"ajouterCommentaire": "Add comment",
		"erreurAjoutCommentaire": "*Fields incorrect",
		"commentaires": "Comments",
		"placeholderCommentaire": "Write a comment",
		"ligneCommentaire": "Line:"  
	}
}
